FROM openjdk:11
RUN mkdir /build
WORKDIR /build
COPY . .
RUN chmod +x mvnw
CMD [ "./mvnw", "spring-boot:run"]


#FROM openjdk:11
#EXPOSE 8080
#ADD target/spring-boot-docker.jar spring-boot-docker.jar
#ENTRYPOINT ["java", "-jar","/spring-boot-docker.jar"]