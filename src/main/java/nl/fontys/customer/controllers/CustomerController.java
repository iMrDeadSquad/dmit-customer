package nl.fontys.customer.controllers;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import nl.fontys.customer.rabbitmq.CustomerSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.fontys.customer.models.Customer;
import nl.fontys.customer.repository.CustomerRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getAllCustomers(@RequestParam(required = false) String companyName) {
        try {
            List<Customer> customers = new ArrayList<Customer>();

            if (companyName == null || companyName.equals(""))
                customerRepository.findAll().forEach(customers::add);
            else
                customerRepository.findByCompanyName(companyName).forEach(customers::add);

            if (customers.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(customers, HttpStatus.OK);
        }   catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable("id") String id) {
        Customer customerData = customerRepository.findByUuid(id);

        if (customerData != null) {
            return new ResponseEntity<>(customerData, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/customers")
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {  //NOSONAR
        try {
            Customer _customer = customerRepository
                    .save(new Customer(customer.getCompanyName(), customer.getFirstName(), customer.getPrefix(), customer.getLastName(), customer.getStreet(), customer.getHouseNumber(), customer.getAddition(), customer.getPostalCode(), customer.getPlace(), customer.getEmail(), customer.getPhoneNumber(), customer.getKvk(), customer.getBtwnr(), customer.getCustomerSinceDate(),customer.getStatus()));
            CustomerSendService.publishNewCustomer(_customer);
            return new ResponseEntity<>(_customer, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<Customer> updateCustomer(@PathVariable("id") String id, @RequestBody Customer customer) throws IOException {  //NOSONAR
        Customer customerData = customerRepository.findByUuid(id);

        if (customerData != null) {
            Customer _customer = customerData;
            _customer.setCompanyName(customer.getCompanyName());
            _customer.setFirstName(customer.getFirstName());
            _customer.setPrefix(customer.getPrefix());
            _customer.setLastName(customer.getLastName());
            _customer.setStreet(customer.getStreet());
            _customer.setHouseNumber(customer.getHouseNumber());
            _customer.setAddition(customer.getAddition());
            _customer.setPostalCode(customer.getPostalCode());
            _customer.setPlace(customer.getPlace());
            _customer.setEmail(customer.getEmail());
            _customer.setPhoneNumber(customer.getPhoneNumber());
            _customer.setKvk(customer.getKvk());
            _customer.setBtwnr(customer.getBtwnr());
            _customer.setCustomerSinceDate(customer.getCustomerSinceDate());
            _customer.setStatus(customer.getStatus());
            CustomerSendService.publishUpdateCustomer(_customer);
            return new ResponseEntity<>(customerRepository.save(_customer), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<HttpStatus> deleteCustomer(@PathVariable("id") long id) {
        try {
            Optional<Customer> customer = customerRepository.findById(id);
            Customer customerRMQ = customerRepository.findByUuid(customer.get().getUuid());
            CustomerSendService.publishDeleteCustomer(customerRMQ);
            customerRepository.deleteById(customerRMQ.getId());
            System.out.println("Customer removed");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/customers")
    public ResponseEntity<HttpStatus> deleteAllCustomer() {
        try {
            customerRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers/active")
    public ResponseEntity<List<Customer>> findByActive() {
        try {
            List<Customer> customers = customerRepository.findByActive(true);

            if (customers.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
