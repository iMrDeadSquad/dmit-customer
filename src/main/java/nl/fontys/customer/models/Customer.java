package nl.fontys.customer.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "customer_uuid")
    private String uuid;

    @NotBlank
    private String companyName;

    private String firstName;

    private String prefix;

    private String lastName;

    private String street;

    private Integer houseNumber;

    private String addition;

    @Size(max = 6)
    private String postalCode;

    private String place;

    @Email
    private String email;

    private Long phoneNumber;

    private String kvk;

    private String btwnr;

    @Temporal(TemporalType.DATE)
    private Date customerSinceDate = new Date();

    @Enumerated(EnumType.STRING)
    private Status status;

    public Customer(String companyName, String firstName, String prefix, String lastName, String street, Integer houseNumber, String addition, String postalCode, String place, String email, Long phoneNumber, String kvk, String btwnr, Date customerSinceDate, Status status) {
        this.uuid = UUID.randomUUID().toString();
        this.companyName = companyName;
        this.firstName = firstName;
        this.prefix = prefix;
        this.lastName = lastName;
        this.street = street;
        this.houseNumber = houseNumber;
        this.addition = addition;
        this.postalCode = postalCode;
        this.place = place;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.kvk = kvk;
        this.btwnr = btwnr;
        this.customerSinceDate = customerSinceDate;
        this.status = status;
    }

}




