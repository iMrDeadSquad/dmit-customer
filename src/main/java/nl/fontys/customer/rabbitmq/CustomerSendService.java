package nl.fontys.customer.rabbitmq;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import io.github.cdimascio.dotenv.Dotenv;
import nl.fontys.customer.models.Customer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class CustomerSendService {

    private static final Dotenv env = Dotenv.load();

    private static final String EXCHANGE_NAME = "customer_exchange";

    private static Channel channel;

    static {
        try {
            if (channel == null) {
                channel = getFactory().newConnection().createChannel();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    private static ConnectionFactory getFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(env.get("RABBITMQ_HOST"));
        factory.setUsername(env.get("RABBITMQ_USER"));
        factory.setPassword(env.get("RABBITMQ_PASSWORD"));
        return factory;
    }

    public static void publishNewCustomer(Customer customer) throws IOException {
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String json = new Gson().toJson(customer);
        channel.basicPublish(EXCHANGE_NAME, "customer.add", null, json.getBytes(StandardCharsets.UTF_8));
    }

    public static void publishDeleteCustomer(Customer customer) throws IOException {
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String json = new Gson().toJson(customer);
        channel.basicPublish(EXCHANGE_NAME, "customer.delete", null, json.getBytes(StandardCharsets.UTF_8));
    }

    public static void publishUpdateCustomer(Customer customer) throws IOException {
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String json = new Gson().toJson(customer);
        channel.basicPublish(EXCHANGE_NAME, "customer.update", null, json.getBytes(StandardCharsets.UTF_8));
    }

    private static void publish(String queue, String message) {
        try {

            // makes sure that the messages are saved for when RabbitMQ stops
            boolean durable = true;
            channel.queueDeclare(queue, durable, false, false, null);
            channel.basicPublish("", queue, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());

        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
