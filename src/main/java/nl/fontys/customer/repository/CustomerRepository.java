package nl.fontys.customer.repository;

import nl.fontys.customer.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long>{
    //List<Customer> findByActive(boolean active);
    //List<Customer> findByCompanyName(String companyName);

    @Query("select c from Customer c " +
            "where lower(c.companyName) like lower(concat('%', :searchTerm, '%')) ")
    List<Customer> findByCompanyName(@Param("searchTerm") String companyName);

    @Query("select c from Customer c " +
            "where lower(c.status) like lower(concat('%', :active, '%')) ")
    List<Customer> findByActive(@Param("active") boolean active);

    Customer findByUuid (String id);

    void deleteById(Optional<Customer> supplier);
}
